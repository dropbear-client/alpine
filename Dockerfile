#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

# alpine uses the tag 'latest' as its latest stable release
# hadolint ignore=DL3007
FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="oliver@schinagl.nl"

# We want the latest stable version of the openssh-client package.
# hadolint ignore=DL3018
RUN \
	apk add --no-cache \
		dropbear-convert \
		dropbear-ssh \
	&& \
	rm -f /var/cache/apk/*

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
