#!/bin/sh
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

COMMANDS=" \
    dropbearconvert \
    ssh \
"

result=0

for cmd in ${COMMANDS}; do
    if ! PATH="${PATH}:/sbin:/usr/sbin:/usr/local/sbin" command -V "${cmd}"; then
        result=1
    fi
done

if [ "${result}" -ne 0 ]; then
    echo "ERROR: missing commands in build environment, cannot continue."
    exit 1
fi

echo "OK: All required commands available."

exit 0
